# How to use

1. Copy example-config.yaml to config.yaml
2. Adjust config.yaml to your liking
3. execute with "perl borgbotlauncher.pl"

# Perl Module Dependencies

- Bot::BasicBot
- Net::Hotline::Client
- YAML::XS
- Socket
- IO::Filename
- utf8
- POE
- IPC::System::Simple

# Commands

- **!userlist** results in the userlist from Hotline or IRC being posted to main chat
